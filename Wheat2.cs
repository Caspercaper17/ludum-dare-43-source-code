//same purpose as Meat2

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wheat2 : MonoBehaviour {

    public static int WheatCount1 = 10;
    Text Wheat1;
    public float TimeSinceLastWheat = 8;
    public int Wheatnodes = 1;


    void Start()
    {
        Wheat1 = GetComponent<Text>();
    }

    void Update()
    {

        if (TimeSinceLastWheat > 0)
        {
            TimeSinceLastWheat = TimeSinceLastWheat - Time.deltaTime;
        }

        else if (TimeSinceLastWheat < 0)
        {
            TimeSinceLastWheat = 8;
            WheatCount1 = WheatCount1 + (1 * Wheatnodes);

        }
        else
        {
            TimeSinceLastWheat = TimeSinceLastWheat - Time.deltaTime;
        }
        Wheat1.text = "(4) Wheat: " + WheatCount1;

    }
}

