//The name makes this script pretty obvious, this uses the time float and convert to and int, I found out htis was possible pretty late in the process, thus the ugly timer and gods opinion.


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Highscore : MonoBehaviour {
    Text GameoverText;
    public static int highscore = 0;


    void Start () {
        GameoverText = GetComponent<Text>();
        GameoverText.text = " ";
    }
	
	// Update is called once per frame
	void Update () {
        if (GameFuctions.GameOver == true)
        {
            GameoverText.text = "Highscore: " + highscore;
        }
        else if
            (GameFuctions.GameOver == false)
        {
            GameoverText.text = "";
        }

        if (highscore < Timer0.time)
        {
            highscore = System.Convert.ToInt32(Timer0.time);
            
           
        }
    }
}