//Same purpose as Meat2

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fruit : MonoBehaviour {

    public static int FruitCount1 = 10;
    Text Fruit1;
    public float TimeSinceLastFruit = 3;
    public int Fruitnodes = 0;


    void Start()
    {
        Fruit1 = GetComponent<Text>();
    }

    void Update()
    {

        if (TimeSinceLastFruit > 0)
        {
            TimeSinceLastFruit = TimeSinceLastFruit - Time.deltaTime;
        }

        else if (TimeSinceLastFruit < 0)
        {
            TimeSinceLastFruit = 3;
            FruitCount1 = FruitCount1 + (1 * Fruitnodes);

        }
        else
        {
            TimeSinceLastFruit = TimeSinceLastFruit - Time.deltaTime;
        }
        Fruit1.text = "(2) Fruit: " + FruitCount1;

    }
}
