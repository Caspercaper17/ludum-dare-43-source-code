//Game.cs file.
//This file contains the code for sacrifces and the game over conditions



using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameFuctions : MonoBehaviour {

    public static float god = 100; //Gods satisfaction
    public static bool GameOver = false; //Game Over
    public static bool EatLeft = false;
    public static bool EatRight = false;
    public float time = 0; // This variable I use in line 94 to do random eating events

    void Start () {


    }
	

	void Update () {



        //Left side

        if (Input.GetKeyDown(KeyCode.Alpha1) && (WheatCounter.WheatCount1 > 10))
        {
            WheatCounter.WheatCount1 = WheatCounter.WheatCount1 - 10;
            god = god + 50;
        }

        if (Input.GetKeyDown(KeyCode.Alpha3) && (Meat.MeatCount1 > 10))
        {
            Meat.MeatCount1 = Meat.MeatCount1 - 10;
            god = god + 50;
        }

        if (Input.GetKeyDown(KeyCode.Alpha2) && (Fruit.FruitCount1 > 10))
        {
            Fruit.FruitCount1 = Fruit.FruitCount1 - 10;
            god = god + 50;
        }

        //Right side

        if (Input.GetKeyDown(KeyCode.Alpha4) && (Wheat2.WheatCount1 > 10))
        {
            Wheat2.WheatCount1 = Wheat2.WheatCount1 - 10;
            god = god + 50;
        }

        if ( Input.GetKeyDown(KeyCode.Alpha6) && (Meat2.MeatCount1 > 10))
        {
            Meat2.MeatCount1 = Meat2.MeatCount1 - 10;
            god = god + 50;
        }

        if (Input.GetKeyDown(KeyCode.Alpha5) && (Fruit2.FruitCount1 > 10))
        {
            Fruit2.FruitCount1 = Fruit2.FruitCount1 - 10;
            god = god + 50;
        }

        // Gods satsfaction slowsly decreasing

        god = god - (Time.deltaTime * 3);

        if ( god < 0)
        {
            GameOver = true;
        }
        else if (WheatCounter.WheatCount1 < 0  || Wheat2.WheatCount1 < 0 || Meat2.MeatCount1 < 0 || Meat.MeatCount1 < 0 || Fruit.FruitCount1 < 0 || Fruit2.FruitCount1 < 0)
        {
            GameOver = true;
        }

        //Consumption of food

        if (EatLeft == true )
        {
            WheatCounter.WheatCount1 = WheatCounter.WheatCount1 - Random.Range(1, 5);
            Meat.MeatCount1 = Meat.MeatCount1 - Random.Range(1, 4);
            Fruit.FruitCount1 = Fruit.FruitCount1 - Random.Range(1, 4);
            EatLeft = false;

        }

        if (EatRight == true)
        {
            Wheat2.WheatCount1 = Wheat2.WheatCount1 - Random.Range(1, 5);
            Meat2.MeatCount1 = Meat2.MeatCount1 - Random.Range(1, 4);
            Fruit2.FruitCount1 = Fruit2.FruitCount1 - Random.Range(1, 4);
            EatRight = false;
        }

        //People and animals randomly eating

        time = time + Time.deltaTime;


        
        if (time > 15)
        {
            EatLeft = true;

        }
        if (time > 15)
        {
            EatRight = true;
            time = 0;
        }

        //Transfer wheat to different sides

        if (Input.GetKey(KeyCode.E) && WheatCounter.WheatCount1 > 1)
        {
            Wheat2.WheatCount1 = Wheat2.WheatCount1 + 1;
            WheatCounter.WheatCount1 = WheatCounter.WheatCount1 - 1;
        }
        if (Input.GetKey(KeyCode.Q) && Wheat2.WheatCount1 > 1)
        {
            Wheat2.WheatCount1 = Wheat2.WheatCount1 - 1;
            WheatCounter.WheatCount1 = WheatCounter.WheatCount1 + 1;
        }


        if (Input.GetKey(KeyCode.D) && Fruit.FruitCount1 > 1)
        {
            Fruit2.FruitCount1 = Fruit2.FruitCount1 + 1;
            Fruit.FruitCount1 = Fruit.FruitCount1 - 1;
        }
        if (Input.GetKey(KeyCode.A) && Fruit2.FruitCount1 > 1)
        {
            Fruit2.FruitCount1 = Fruit2.FruitCount1 - 1;
            Fruit.FruitCount1 = Fruit.FruitCount1 + 1;
        }

        if (Input.GetKey(KeyCode.C) && Meat.MeatCount1 > 1)
        {
            Meat2.MeatCount1 = Meat2.MeatCount1 + 1;
            Meat.MeatCount1 = Meat.MeatCount1 - 1;
        }
        if (Input.GetKey(KeyCode.Z) && Meat2.MeatCount1 > 1)
        {
            Meat2.MeatCount1 = Meat2.MeatCount1 - 1;
            Meat.MeatCount1 = Meat.MeatCount1 + 1;
        }

        //Reload and back to main menu

        if (Input.GetKey(KeyCode.R))
        {
            int scene = SceneManager.GetActiveScene().buildIndex;
            SceneManager.LoadScene(scene, LoadSceneMode.Single);

            god = 100;
            Meat2.MeatCount1 = 10;
            Meat.MeatCount1 = 10;
            Fruit.FruitCount1 = 10;
            Fruit2.FruitCount1 = 10;
            WheatCounter.WheatCount1 = 10;
            Wheat2.WheatCount1 = 10;
            GameOver = false;
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            int scene = 0;
            SceneManager.LoadScene(scene, LoadSceneMode.Single);

        }

    }
}
