//The .cs file is normally the game.cs file, made an mistake with the website ;O
//This is the games timer

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer0 : MonoBehaviour {

    Text Timer;
    public static float time = 0;
    public static bool Stopped = false;


    void Start ()
    {
    Timer = GetComponent<Text>();
    }

	void Update () {

        if ( GameFuctions.GameOver != true)
        {
            time = time + Time.deltaTime ;
        
        }
        else
        {
            Stopped = true;
        }
        Timer.text = "Time: " + time;
	}
}
