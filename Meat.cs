//Same purpose as meat2

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Meat : MonoBehaviour
{

    public static int MeatCount1 = 10;
    Text Meat1;
    public float TimeSinceLastMeat = 10;
    public int Meatnodes = 0;


    void Start()
    {
        Meat1 = GetComponent<Text>();
    }

    void Update()
    {

        if (TimeSinceLastMeat > 0)
        {
            TimeSinceLastMeat = TimeSinceLastMeat - Time.deltaTime;
        }

        else if (TimeSinceLastMeat < 0)
        {
            TimeSinceLastMeat = 10;
            MeatCount1 = MeatCount1 + (1 * Meatnodes);

        }
        else
        {
            TimeSinceLastMeat = TimeSinceLastMeat - Time.deltaTime;
        }
        Meat1.text = "(3) Meat: " + MeatCount1;

    }
}