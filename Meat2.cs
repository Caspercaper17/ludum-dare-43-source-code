//A script makeing sure the amount of meat increases on the left side.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Meat2 : MonoBehaviour {

    public static int MeatCount1 = 10;
    Text Meat1;
    public float TimeSinceLastMeat = 8;
    public int Meatnodes = 0;


    void Start()
    {
        Meat1 = GetComponent<Text>();
    }

    void Update()
    {

        if (TimeSinceLastMeat > 0)
        {
            TimeSinceLastMeat = TimeSinceLastMeat - Time.deltaTime;
        }

        else if (TimeSinceLastMeat < 0)
        {
            TimeSinceLastMeat = 8;
            MeatCount1 = MeatCount1 + (1 * Meatnodes);

        }
        else
        {
            TimeSinceLastMeat = TimeSinceLastMeat - Time.deltaTime;
        }
        Meat1.text = "(6) Meat: " + MeatCount1;

    }
}